package com.example.demo.repo;

import com.example.demo.domain.Person;
import com.example.demo.domain.RoomBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public interface RoomBookingRepository extends JpaRepository<RoomBooking, Long> {
    List<RoomBooking> findByRoomAndTime(int room, Instant time);
}
