
package com.example.demo.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.time.Instant;

@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = true)
public class RoomBooking extends AbstractEntity {

    private int room;

    private Instant time;

}
