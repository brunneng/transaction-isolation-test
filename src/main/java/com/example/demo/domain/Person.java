package com.example.demo.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = true)
public class Person extends AbstractEntity {

    private String name;

    private int age;

}
