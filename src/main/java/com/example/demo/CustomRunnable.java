package com.example.demo;

import java.util.concurrent.BrokenBarrierException;

@FunctionalInterface
public interface CustomRunnable {
    void run() throws InterruptedException, BrokenBarrierException;
}
