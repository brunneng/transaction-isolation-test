package com.example.demo.service;

import com.example.demo.CustomRunnable;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;

@Service
@RequiredArgsConstructor
public class TransactionService {

    @SneakyThrows
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void runInTransactionReadUncommitted(CustomRunnable runnable) {
        runnable.run();
    }

    @SneakyThrows
    @Transactional(isolation = Isolation.READ_COMMITTED)
    public void runInTransactionReadCommitted(CustomRunnable runnable) {
        runnable.run();
    }

    @SneakyThrows
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void runInTransactionRepeatableRead(CustomRunnable runnable) {
        runnable.run();
    }

    @SneakyThrows
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void runInTransactionSerializable(CustomRunnable runnable) {
        runnable.run();
    }

    @SneakyThrows
    @Transactional(isolation = Isolation.DEFAULT)
    public void runInTransactionDefault(CustomRunnable runnable) {
        runnable.run();
    }

    public void rollbackTransaction() {
        TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
    }
}
