package com.example.demo.repo.unrepread;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("oracle")
public class OracleUnrepeatableReadTest extends UnrepeatableReadTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        useOracleDb(registry);
    }
}
