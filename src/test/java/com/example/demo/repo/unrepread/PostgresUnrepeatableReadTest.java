package com.example.demo.repo.unrepread;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("postgres")
public class PostgresUnrepeatableReadTest extends UnrepeatableReadTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        usePostgresDb(registry);
    }
}
