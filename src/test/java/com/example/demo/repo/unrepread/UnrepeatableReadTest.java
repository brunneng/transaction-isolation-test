package com.example.demo.repo.unrepread;

import com.example.demo.BaseIsolationTest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.transaction.annotation.Isolation;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public abstract class UnrepeatableReadTest extends BaseIsolationTest {

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testReadResultIsNotChangedWriteStartedAfter(Isolation readIsolation) {
        var latch1 = createCountDownLatch1();
        var latch2 = createCountDownLatch1();

        var p = personRepository.saveAndFlush(createPerson());

        runInSeparateThread(() -> {
            latch1.await();

            runInTransaction(Isolation.READ_COMMITTED, () -> {
                var localPerson = personRepository.searchById(p.getId());
                assertThat(localPerson).isNotSameAs(p);
                localPerson.setName(DIFFERENT_NAME);
                personRepository.save(localPerson);
            });

            log.info("2");
            latch2.countDown();
        });

        runInTransaction(readIsolation, () -> {
            var localPerson = personRepository.searchById(p.getId());
            assertThat(localPerson.getName()).isNotEqualTo(DIFFERENT_NAME);
            entityManager.detach(localPerson);
            log.info("1");

            latch1.countDown();
            awaitWithTimeout(latch2);

            localPerson = personRepository.searchById(p.getId());
            assertThat(localPerson.getName()).isNotEqualTo(DIFFERENT_NAME);
            log.info("3");
        });

        assertThat(personRepository.searchById(p.getId()).getName()).isEqualTo(DIFFERENT_NAME);
    }

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testReadResultIsNotChangedWriteStartedBefore(Isolation readIsolation) {
        var latch1 = createCountDownLatch1();
        var latch2 = createCountDownLatch1();
        var latch3 = createCountDownLatch1();

        var p = personRepository.saveAndFlush(createPerson());

        runInSeparateThread(() -> {
            runInTransaction(Isolation.READ_COMMITTED, () -> {
                log.info("1");
                latch1.countDown();
                latch2.await();

                var localPerson = personRepository.searchById(p.getId());
                assertThat(localPerson).isNotSameAs(p);
                localPerson.setName(DIFFERENT_NAME);
                personRepository.save(localPerson);
            });

            log.info("3");
            latch3.countDown();
        });

        latch1.await();

        runInTransaction(readIsolation, () -> {
            var localPerson = personRepository.searchById(p.getId());
            assertThat(localPerson.getName()).isNotEqualTo(DIFFERENT_NAME);
            entityManager.detach(localPerson);
            log.info("2");

            latch2.countDown();
            awaitWithTimeout(latch3);

            localPerson = personRepository.searchById(p.getId());
            assertThat(localPerson.getName()).isNotEqualTo(DIFFERENT_NAME);
            log.info("4");
        });

        assertThat(personRepository.searchById(p.getId()).getName()).isEqualTo(DIFFERENT_NAME);
    }
}
