package com.example.demo.repo.unrepread;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("mssql")
public class MsSqlUnrepeatableReadTest extends UnrepeatableReadTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        useMsSqlDb(registry);
    }
}
