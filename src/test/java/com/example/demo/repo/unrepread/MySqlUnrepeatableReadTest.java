package com.example.demo.repo.unrepread;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("mysql")
public class MySqlUnrepeatableReadTest extends UnrepeatableReadTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        useMySqlDb(registry);
    }
}
