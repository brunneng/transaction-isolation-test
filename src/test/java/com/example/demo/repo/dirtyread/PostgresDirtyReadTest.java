package com.example.demo.repo.dirtyread;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("postgres")
public class PostgresDirtyReadTest extends DirtyReadTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        usePostgresDb(registry);
    }
}
