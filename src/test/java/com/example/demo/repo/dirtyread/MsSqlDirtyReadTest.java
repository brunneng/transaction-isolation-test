package com.example.demo.repo.dirtyread;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("mssql")
public class MsSqlDirtyReadTest extends DirtyReadTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        useMsSqlDb(registry);
    }
}
