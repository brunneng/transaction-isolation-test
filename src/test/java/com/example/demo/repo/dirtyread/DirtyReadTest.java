package com.example.demo.repo.dirtyread;

import com.example.demo.BaseIsolationTest;
import com.example.demo.IdHolder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.transaction.annotation.Isolation;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public abstract class DirtyReadTest extends BaseIsolationTest {

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testNoDirtyReadOfCreatedPerson(Isolation readIsolation) {
        var latch1 = createCountDownLatch1();
        var latch2 = createCountDownLatch1();

        var idHolder = new IdHolder();
        runInSeparateThread(() -> {
            runInTransaction(Isolation.READ_COMMITTED, () -> {
                var p = personRepository.saveAndFlush(createPerson());
                idHolder.setId(p.getId());
                log.info("1");

                latch1.countDown();
                awaitWithTimeout(latch2);
                transactionService.rollbackTransaction();
                log.info("4");
            });
        });

        latch1.await();
        try {
            runInTransaction(readIsolation, () -> {
                log.info("2");
                assertThat(personRepository.existsById(idHolder.getId())).isFalse();
                log.info("3");
            });
        }
        finally {
            latch2.countDown();
        }
        checkExceptionInThread();
    }

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testNoDirtyReadOfUpdatedPerson(Isolation readIsolation) {
        var latch1 = createCountDownLatch1();
        var latch2 = createCountDownLatch1();

        var p = personRepository.saveAndFlush(createPerson());

        runInSeparateThread(() -> {
            runInTransaction(Isolation.READ_COMMITTED, () -> {
                p.setName(DIFFERENT_NAME);
                personRepository.saveAndFlush(p);
                log.info("1");

                latch1.countDown();
                awaitWithTimeout(latch2);
                transactionService.rollbackTransaction();
                log.info("4");
            });
        });

        latch1.await();
        try {
            runInTransaction(readIsolation, () -> {
                log.info("2");
                assertThat(personRepository.searchById(p.getId()).getName()).isNotEqualTo(DIFFERENT_NAME);
                log.info("3");
            });
        }
        finally {
            latch2.countDown();
        }
        checkExceptionInThread();
    }

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testNoDirtyReadOfDeletedPerson(Isolation readIsolation) {
        var latch1 = createCountDownLatch1();
        var latch2 = createCountDownLatch1();

        var p = personRepository.saveAndFlush(createPerson());

        runInSeparateThread(() -> {
            runInTransaction(Isolation.READ_COMMITTED, () -> {
                personRepository.delete(p);
                personRepository.flush();
                log.info("1");

                latch1.countDown();
                awaitWithTimeout(latch2);
                transactionService.rollbackTransaction();
                log.info("4");
            });
        });

        latch1.await();
        try {
            runInTransaction(readIsolation, () -> {
                log.info("2");
                assertThat(personRepository.existsById(p.getId())).isTrue();
                log.info("3");
            });
        }
        finally {
            latch2.countDown();
        }
        checkExceptionInThread();
    }
}
