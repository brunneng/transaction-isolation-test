package com.example.demo.repo.phantomread;

import com.example.demo.BaseIsolationTest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.transaction.annotation.Isolation;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public abstract class PhantomReadTest extends BaseIsolationTest {

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testNoPhantomReadOnCreateWhenWriteStartedAfter(Isolation readIsolation) {
        var latch1 = createCountDownLatch1();
        var latch2 = createCountDownLatch1();

        runInSeparateThread(() -> {
            latch1.await();
            runInTransaction(Isolation.READ_COMMITTED, () -> {
                personRepository.saveAndFlush(createPerson());
            });
            log.info("2");
            latch2.countDown();
        });

        runInTransaction(readIsolation, () -> {
            log.info("1");
            assertThat(personRepository.findByNameContains(ORIGINAL_NAME)).isEmpty();

            latch1.countDown();
            awaitWithTimeout(latch2);

            assertThat(personRepository.findByNameContains(ORIGINAL_NAME)).isEmpty();
            log.info("3");
        });

        checkExceptionInThread();
    }

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testNoPhantomReadOnCreateWhenWriteStartedBefore(Isolation readIsolation) {
        var latch1 = createCountDownLatch1();
        var latch2 = createCountDownLatch1();
        var latch3 = createCountDownLatch1();

        runInSeparateThread(() -> {
            runInTransaction(Isolation.READ_COMMITTED, () -> {
                log.info("1");

                latch1.countDown();
                latch2.await();
                personRepository.saveAndFlush(createPerson());
            });
            log.info("3");
            latch3.countDown();
        });

        latch1.await();
        runInTransaction(readIsolation, () -> {
            log.info("2");
            assertThat(personRepository.findByNameContains(ORIGINAL_NAME)).isEmpty();

            latch2.countDown();
            awaitWithTimeout(latch3);

            assertThat(personRepository.findByNameContains(ORIGINAL_NAME)).isEmpty();
            log.info("4");
        });

        checkExceptionInThread();
    }

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testNoPhantomReadOnUpdateWhenWriteStartedAfter(Isolation readIsolation) {
        var latch1 = createCountDownLatch1();
        var latch2 = createCountDownLatch1();

        var p = personRepository.saveAndFlush(createPerson());

        runInSeparateThread(() -> {
            latch1.await();
            runInTransaction(Isolation.READ_COMMITTED, () -> {
                var person = personRepository.searchById(p.getId());
                person.setName(DIFFERENT_NAME);
                personRepository.save(person);
            });
            log.info("2");
            latch2.countDown();
        });

        runInTransaction(readIsolation, () -> {
            log.info("1");
            assertThat(personRepository.findByNameContains(ORIGINAL_NAME)).hasSize(1);

            latch1.countDown();
            awaitWithTimeout(latch2);

            assertThat(personRepository.findByNameContains(ORIGINAL_NAME)).hasSize(1);
            log.info("3");
        });

        checkExceptionInThread();
    }

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testNoPhantomReadOnUpdateWhenWriteStartedBefore(Isolation readIsolation) {
        var latch1 = createCountDownLatch1();
        var latch2 = createCountDownLatch1();
        var latch3 = createCountDownLatch1();

        var p = personRepository.saveAndFlush(createPerson());

        runInSeparateThread(() -> {
            runInTransaction(Isolation.READ_COMMITTED, () -> {
                log.info("1");

                latch1.countDown();
                latch2.await();

                var person = personRepository.searchById(p.getId());
                person.setName(DIFFERENT_NAME);
                personRepository.save(person);
            });
            log.info("3");
            latch3.countDown();
        });

        latch1.await();
        runInTransaction(readIsolation, () -> {
            log.info("2");
            assertThat(personRepository.findByNameContains(ORIGINAL_NAME)).hasSize(1);

            latch2.countDown();
            awaitWithTimeout(latch3);

            assertThat(personRepository.findByNameContains(ORIGINAL_NAME)).hasSize(1);
            log.info("4");
        });

        checkExceptionInThread();
    }
}
