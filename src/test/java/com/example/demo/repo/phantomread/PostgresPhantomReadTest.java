package com.example.demo.repo.phantomread;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("postgres")
public class PostgresPhantomReadTest extends PhantomReadTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        usePostgresDb(registry);
    }
}
