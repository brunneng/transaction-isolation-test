package com.example.demo.repo.lostupdate;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("postgres")
public class PostgresLostUpdateTest extends LostUpdateTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        usePostgresDb(registry);
    }
}
