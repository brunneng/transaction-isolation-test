package com.example.demo.repo.lostupdate;

import com.example.demo.BaseIsolationTest;
import lombok.SneakyThrows;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.transaction.annotation.Isolation;

import java.util.concurrent.CountDownLatch;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class LostUpdateTest extends BaseIsolationTest {

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testNoLostUpdate(Isolation isolation) {
        var threadsCount = 2;
        var latch1 = new CountDownLatch(threadsCount);
        var latch2 = new CountDownLatch(threadsCount);

        var p = personRepository.saveAndFlush(createPerson());
        for (int i = 0; i < threadsCount; ++i) {
            runInSeparateThread(() -> {
                try {
                    runInTransaction(isolation, () -> {
                        latch1.countDown();
                        latch1.await();

                        var personToUpdate = personRepository.searchById(p.getId());
                        personToUpdate.setAge(personToUpdate.getAge() + 1);
                        personRepository.save(personToUpdate);
                    });
                } finally {
                    latch2.countDown();
                }
            });
        }

        latch2.await();
        assertThat(personRepository.searchById(p.getId()).getAge()).isEqualTo(p.getAge() + threadsCount);
    }

}
