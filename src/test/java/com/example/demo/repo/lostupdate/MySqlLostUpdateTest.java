package com.example.demo.repo.lostupdate;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("mysql")
public class MySqlLostUpdateTest extends LostUpdateTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        useMySqlDb(registry);
    }
}
