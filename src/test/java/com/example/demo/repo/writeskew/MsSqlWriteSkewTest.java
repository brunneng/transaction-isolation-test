package com.example.demo.repo.writeskew;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("mssql")
public class MsSqlWriteSkewTest extends WriteSkewTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        useMsSqlDb(registry);
    }
}
