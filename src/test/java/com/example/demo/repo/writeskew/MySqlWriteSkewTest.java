package com.example.demo.repo.writeskew;

import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;

@ActiveProfiles("mysql")
public class MySqlWriteSkewTest extends WriteSkewTest {
    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        useMySqlDb(registry);
    }
}
