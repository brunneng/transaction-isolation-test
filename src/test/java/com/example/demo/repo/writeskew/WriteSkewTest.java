package com.example.demo.repo.writeskew;

import com.example.demo.BaseIsolationTest;
import com.example.demo.domain.RoomBooking;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.springframework.transaction.annotation.Isolation;

import java.time.Instant;
import java.util.concurrent.CountDownLatch;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public abstract class WriteSkewTest extends BaseIsolationTest {

    @SneakyThrows
    @ParameterizedTest
    @EnumSource(Isolation.class)
    public void testNoWriteSkew(Isolation isolation) {
        var threadsCount = 2;
        var latch1 = new CountDownLatch(threadsCount);
        var latch2 = new CountDownLatch(threadsCount);

        for (int i = 0; i < threadsCount; ++i) {
            runInSeparateThread(() -> {
                try {
                    runInTransaction(isolation, () -> {
                        latch1.countDown();
                        latch1.await();

                        var roomBooking = new RoomBooking();
                        roomBooking.setRoom(42);
                        roomBooking.setTime(Instant.parse("2021-11-03T10:15:30.00Z"));

                        if (roomBookingRepository.findByRoomAndTime(
                                roomBooking.getRoom(), roomBooking.getTime()).isEmpty()) {
                            roomBookingRepository.save(roomBooking);
                            log.info("Saved");
                        } else {
                            log.info("Not saved");
                        }
                    });
                } finally {
                    latch2.countDown();
                }
            });
        }

        latch2.await();
        assertThat(roomBookingRepository.findAll()).hasSize(1);
    }

}
