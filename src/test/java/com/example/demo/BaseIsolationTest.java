package com.example.demo;

import com.example.demo.domain.Person;
import com.example.demo.repo.PersonRepository;
import com.example.demo.repo.RoomBookingRepository;
import com.example.demo.service.TransactionService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.transaction.annotation.Isolation;
import org.testcontainers.containers.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Slf4j
@SpringBootTest
public abstract class BaseIsolationTest {

    public static final String ORIGINAL_NAME = "Joe";
    public static final String DIFFERENT_NAME = "Billy";

    public static final MySQLContainer mysql = new MySQLContainer<>("mysql:5.7.34");
    public static final PostgreSQLContainer postgres = new PostgreSQLContainer<>("postgres:9.6.12");
    public static final OracleContainer oracle = new OracleContainer("gvenzl/oracle-xe:18.4.0")
            .withEnv("ORACLE_PASSWORD", "oracle");
    public static MSSQLServerContainer mssql = new MSSQLServerContainer<>(
            "mcr.microsoft.com/mssql/server:2017-CU12").acceptLicense();

    protected static void useMySqlDb(DynamicPropertyRegistry registry) {
        useDb(mysql, registry);
    }

    protected static void usePostgresDb(DynamicPropertyRegistry registry) {
        useDb(postgres, registry);
    }

    protected static void useOracleDb(DynamicPropertyRegistry registry) {
        useDb(oracle, registry);
    }

    protected static void useMsSqlDb(DynamicPropertyRegistry registry) {
        useDb(mssql, registry);
    }

    protected static void useDb(JdbcDatabaseContainer<?> container, DynamicPropertyRegistry registry) {
        container.start();
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.username", container::getUsername);
        registry.add("spring.datasource.password", container::getPassword);
        registry.add("spring.datasource.driver-class-name", container::getDriverClassName);
    }

    @Autowired
    protected PersonRepository personRepository;

    @Autowired
    protected RoomBookingRepository roomBookingRepository;

    @Autowired
    protected TransactionService transactionService;

    @PersistenceContext
    protected EntityManager entityManager;

    private Thread thread;

    private RuntimeException exceptionInThread;

    @AfterEach
    public void afterEach() {
        personRepository.deleteAll();
        roomBookingRepository.deleteAll();

        thread = null;
        exceptionInThread = null;
    }

    protected void runInSeparateThread(CustomRunnable runnable) {
        thread = new Thread(() -> {
            try {
                runnable.run();
            } catch (Throwable e) {
                exceptionInThread = new RuntimeException(e);
                throw exceptionInThread;
            }
        });
        thread.start();
    }

    @SneakyThrows
    protected void checkExceptionInThread() {
        thread.join();
        if (exceptionInThread != null) {
            throw exceptionInThread;
        }
    }

    protected Person createPerson() {
        var person = new Person();
        person.setName(ORIGINAL_NAME);
        person.setAge(30);
        return person;
    }

    protected void runInTransaction(Isolation isolation, CustomRunnable runnable) {
        try {
            if (isolation == Isolation.READ_UNCOMMITTED) {
                transactionService.runInTransactionReadUncommitted(runnable);
            } else if (isolation == Isolation.READ_COMMITTED) {
                transactionService.runInTransactionReadCommitted(runnable);
            } else if (isolation == Isolation.REPEATABLE_READ) {
                transactionService.runInTransactionRepeatableRead(runnable);
            } else if (isolation == Isolation.SERIALIZABLE) {
                transactionService.runInTransactionSerializable(runnable);
            } else if (isolation == Isolation.DEFAULT) {
                transactionService.runInTransactionDefault(runnable);
            }
        } catch (CannotAcquireLockException | JpaSystemException e) {
          log.error("Error while executing {} transaction. Will retry...", isolation, e);
          runInTransaction(isolation, runnable);
        }

    }

    protected CountDownLatch createCountDownLatch1() {
        return new CountDownLatch(1);
    }

    @SneakyThrows
    protected void awaitWithTimeout(CountDownLatch latch2) {
        if (!latch2.await(2, TimeUnit.SECONDS)) {
            log.warn("Timout detected!");
        }
    }
}
