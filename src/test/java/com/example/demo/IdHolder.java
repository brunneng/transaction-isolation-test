package com.example.demo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IdHolder {
    private Long id;
}
